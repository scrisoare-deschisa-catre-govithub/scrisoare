Scrisoare deschisă către domnul prim-ministru Dacian Cioloș despre inițiativa GovITHub
===

Anunț: între timp pe GovITHub a fost publicată [o listă cu întrebări frecvente](http://ithub.gov.ro/faq/) (FAQ – Frequently Asked Questions) care clarifică cele două întrebări principale:

Citat:

> Q: Se poate participa în GovITHub de la distanță?
> A: Prezența la București este necesară doar pentru cele 10 poziții de fellowship. Pentru pozițiile de voluntari (8h/săptămână), nu este necesară prezența la București — pot fi voluntari atât persoane din țară cât și din diaspora. Locația nu este un criteriu de selecție pentru aceste poziții sau pentru a face parte din comunitatea GovITHub. 

Și:

> Q: Cum va promova GovITHub cultura open source și open data?
> A: Toate proiectele dezvoltate în cadrul GovITHub vor fi open-source. (...)

Deși ar fi fost de preferat să folosească ”programe libere” în loc de open-source, acesta este o discuție mai puțin potrivită pentru o scrisoare deschisă.

Baftă membrilor GovITHub și sper să pot să contribui la acest efort în viitor.
